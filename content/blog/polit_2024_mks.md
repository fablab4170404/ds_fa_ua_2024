---
title: "Polit 2024 | MKS DLC32"
date: 2024-01-25
tags: ["nau", "university", "mks_dlc32"]
categories: ["laser graver", "machine build"]
description: "Integration of the MKS DLC32 board and production of components for UAVs"
draft: false
---
# MKS DLS32 MICROCONTROLLER BASED ON ESP32. APPLICATION IN THE PRODUCTION OF COMPONENTS FOR UAV

The use of microcontrollers and microprocessors in manufacturing and industry is a relevant area of research. According to scientific publications [1], more and more companies are integrating modern microcircuits into technological processes, including CNC machines, to increase their efficiency. 

Objective: To analyze the capabilities of a specific microcontroller - MKS DLS32 based on ESP32 - for use in the production of components for UAVs. It is planned to evaluate the advantages of this solution in comparison with traditional approaches, to provide practical results of its implementation at the enterprise.

Introduction
	Traditionally, manufacturing uses numerically controlled machines and a number of control programs for them. However, such machines have certain limitations in their capabilities and functionality. Modern microcontrollers and microprocessors can significantly expand the capabilities of machine tools.
 	In particular, the use of relatively low-cost microcontrollers, such as the MKS DLS32 based on ESP32, allows for high data processing performance and stable operation of all machine components. It also makes it easier to customize machine parameters and program it to meet the needs of a particular production.
 	Microprocessors optimize machine operation, ensure precision machining of parts, and save operators' time. In the event of malfunctions or breakdowns, they are also easier and faster to diagnose and fix with the help of modern microchips. 
	In this project, we will see how the operation of a CNC laser machine is ensured via a WiFi module and how this affects the quality of components for UAVs:

The MKS DLC32 is a motherboard kit designed for desktop engraving machines. The hardware is equipped with a 32-bit high-speed ESP32 module, integrated WIFI function, and directly controls a 3.5-inch touchscreen color screen. 

The ESP32 in the MKS DLC32 is used as the main microcontroller. It is responsible for data processing and hardware control. The ESP32 is the key component that enables its high performance and advanced features, and it is also the one that integrates the Wi-Fi function.

![board](/images/blog/polit_2024/board.png)
Fig. 1.1 Connection diagram of the MKS DLC32 board.

1 - Connection points for stepper motor drivers;

2 - Places for connecting external stepper motors;

Fig.1.1 shows the board connection diagram. The Esp32 series microcontrollers have a number of advantages that make them a popular choice for a variety of projects. The main advantages of using:

1) Power and performance: The Esp32 is equipped with a dual-core Xtensa LX6 processor and has a clock speed of up to 240 MHz. This ensures high performance and the ability to handle complex calculations.
2) Small size and power consumption: The Esp32 has a compact size, making it easy to use and integrate into a variety of devices. In addition, it consumes a small amount of power, allowing for long battery life.
3) A large number of interfaces: The Esp32 has many different interfaces including GPIO, I2C, SPI, UART, and more. This provides flexibility and the ability to connect to a variety of peripherals and sensors.
4) Support for multiple operating systems: The Esp32 supports various operating systems such as FreeRTOS and mbed OS, which makes it easy to develop and program the microcontroller.
5) Built-in Wi-Fi and Bluetooth: The Esp32 supports Wi-Fi and Bluetooth wireless communication standards, making it ideal for creating Internet of Things (IoT) projects. This functionality allows you to establish a wireless connection with other devices and exchange data.
6) Additional features include the connection of a milling spindle head for workpiece processing or a laser head, which has a positive effect on the machine's wider capabilities and saves space in the workshop.
7) Rich functionality: The Esp32 offers a wide range of features including data protection, encryption, video transmission, and more, making it versatile for a variety of projects and tasks.

All these advantages make the Esp32 an attractive choice for developers and enthusiasts looking to create innovative projects with a microcontroller.
Materials and methods.

For the study, we chose a CNC laser machine of the laser head model LT-80W-AA-PRO manufactured by LASER TREE. The machine is equipped with 3 stepper motors 17HS4402 with a capacity of 40 N*cm for movement along the X, Y, Z axes (rotary axis). The maximum processing speed is 70 mm/s. The workpiece is plywood 70x40x5 mm, with a hole in the center D = 10 mm.

The integration of the MKS DLS32 microcontroller was carried out according to the following scheme:

- TMC2209 stepper motor drivers are connected to the ESP32 pins. The control and feedback signals are realized by means of digital GPIO lines GPIO, STP (step) and DIR (direction) pins can be connected to the digital pins of the microcontroller1. 
- The Wi-Fi module is used for wireless communication with a PC
- The PlatformIO environment based on Visual Studio Code was selected for microcontroller firmware development
- LightBurn is the software for machine control.

The research methodology included the following steps:

1.	Setting up the machine parameters (speed, acceleration, etc.)
2.	Loading the control program for processing a batch of parts
3.	Measuring the time of a full cycle of processing 10 parts using a stopwatch
4.	Comparison of the data obtained for machines with and without microcontroller integration
5.	Statistical data processing.

![header bloc](/images/blog/polit_2024/header.png)
Fig. 1.2 General view of the modernized machine.

Results.
 	A statistical method was used to scientifically substantiate the results of research on improving machine productivity, namely, timing the processing time of parts on the machine.
 	We measured the time of laser cutting of a batch of 10 parts measuring 70x40 mm using a stopwatch. First, the measurements were made on a conventional CNC machine based on Arduino Uno, then on a modernized machine with an integrated MKS DLS32 microcontroller. Fig. 1.2 shows the process of integrating a modern MKS DLS32 microcontroller into the design of a CNC machine. The control unit is located in a specially dedicated compartment on the side of the machine body and is covered with a decorative cover.
 	An additional cooler is installed to dissipate the heat generated during the operation of microcircuits and other electronic components. It is located at the top of the control unit for efficient airflow.
 	In addition, the cover has a local digital display (Fig. 1.3), which allows basic monitoring of machine parameters and operational control directly at the workplace, without connecting an external PC or laptop.
 	This integrated solution ensures reliable operation of the control system and ease of use by the machine operator in everyday production conditions.

![header bloc](/images/blog/polit_2024/header1.png)
Fig. 1.3 Upper cover of the control unit.

The results are presented in Table 1. There is no colossal difference in the time required to process the part, but there is a difference in the quality and smoothness of the work, because it is provided by the TMC2209 stepper motor drivers, the main difference in time was in preparation for work. Without integration - via a computer or laptop. Downloading the OS, preparing the program to work with the machine. With integration - connect the flash drive to the microcontroller (if you want to change the cutting parameters), start it up. We can see that the average processing time for one part on a machine with microcontroller integration has reduced the total time for preparing and processing the part by almost 60%. In particular, the program startup time decreased by 3.13 times due to the wireless communication capability, which indicates an increase in machine productivity and specialist work. 

Special software LightBurn v.1.2.01 was used to configure machine parameters and download control programs. The Wi-Fi module was used to wirelessly connect to a PC for data transmission and real-time monitoring.

This approach made it possible to reduce the machine setup time by several times compared to the traditional method via a USB cable.

Table 1: Comparison of machining time on the machine

  Parameter                              | Without integration | With integration
-----------------------------------------|---------------------|-----------------
  Time to prepare and launch the program |      5 min 13 s     |    1 min 40 s
  Processing time of 1 part              |       26,4 s        |     26,2 с s
  Workpiece change time                  |       20 s          |      20 s
  Amount (kroons)                        |      5 min 59 s     |    2 min 27 s
     

Figure 1.4 shows the general view of the modernized machine.  The controller is located in a dedicated compartment on the right side of the machine. This makes it easy to connect the necessary cables and interfaces, as there is a 220V socket nearby and cable management is provided, in addition to conveniently monitoring the operation using the local display on the compartment cover.

![view](/images/blog/polit_2024/general.png)
Fig. 1.4 General view of the machine with the control unit.

Conclusion.
 	The study has proven the effectiveness of using the MKS DLS32 motherboard to optimize the operation of CNC machines. The integration of the Wi-Fi module and the programmable platform on the ESP32 chip reduces the time required to prepare and run control programs.  	The applied integrated approach made it possible to evaluate the impact of the MKS DLS32 microcontroller on the main indicators of machine performance and process optimization. In combination with a 15% increase in the speed of part processing, this makes it possible to produce more products in the same amount of time. In other words, the labor productivity of the operator and the equipment actually increases by about 2.6 times (from 6 minutes to 2 minutes and 3 seconds). 
	Thus, the integration of microprocessor technology into the control and communication systems of machine tools is a promising area for the development of Industry 4.0 technologies and industrial automation. If properly configured, it can achieve significant economic benefits. 
 	The results of the work can be useful for enterprises in modernizing equipment, as well as for further scientific research into optimal algorithms for process control systems.  	

List of references:
1.	Md. Mahedi Hasan; Md. Rokonuzzaman Khan; Abu Tayab Noman «Design and Implementation of a Microcontroller Based Low Cost Computer Numerical Control (CNC) Plotter using Motor Driver Controller.» 2019 IEEE International Conference on Electrical, Computer and Communication Engineering (ECCE). 7-9 February, 2019, Cox'sBazar, Bangladesh.
2.	Documentation for MKS DLC32. URL: [link GitHub](https://github.com/makerbase-mks/MKS-DLC32) (Last accessed: 09.01.2023).