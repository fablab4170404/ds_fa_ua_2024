---
title: "week 5. 3D scanning and printing"
tags: ["CAD", "CAM", "3D modeling", "3D scanning", "3D printing"]
categories: ["3d_scan", "3d_printer", "repair"]
description: "The process of 3D scanning of objects and their production on a 3D printer."
date: 2024-02-21
draft: false
---

## THE ASSIGNMENT:
1. GROUP ASSIGNMENT: [TEST THE DESIGN RULES FOR YOUR 3D PRINTER](https://gitlab.com/ssavito1122/ds_fa_ua_2024)

2. INDIVIDUAL ASSIGNEMENT: 
- DESIGN AND 3D PRINT AN OBJECT (SMALL, FEW CM3, LIMITED BY PRINTER TIME) THAT COULD NOT BE MADE SUBTRACTIVELY 
- 3D SCAN AN OBJECT (AND OPTIONALLY PRINT IT)

## 1. Workshop
While working on an interesting topic related to 3D printing, I am additionally studying Fusion360 in the process of 3D modeling, and I also want to share with you a tutorial that helped me:
- [YouTube Tutorial](https://www.youtube.com/watch?v=3Z3t7J3Y3Zc)

I use two ways to build a figure, so I can choose which one is more appropriate for the task at hand.

![Version1](/images/week5/fusion_v1.png)

and

![Version2](/images/week5/fusion_v2.png)

Taking into account the tolerances, I chose the following parameters:

![Parameters](/images/week5/dopusk_v2.png)

When we have built our models, we export them in STL format. 

### Software and settings

For FDM I use the free Ultimaker Cura slicer, which has my printer in it. But you need to register there.
 - [Free download Ultimaker Cura](https://ultimaker.com/software/ultimaker-cura/)

Several parameters are important for the the quality of the print and the performance of the 3D printer and all of this is set in the software.
 ![Version1](/images/week5/print1.png)
    AND
 ![Version2](/images/week5/print2.png)

### Designed 
Here's how I approached designing and 3D printing an object:

I began by conducting research on structures and models that are uniquely achievable through additive manufacturing/3D printing. This led me to discover examples of intricate geometric designs, such as those created with topology optimization software. These software tools allow for the creation of complex structures optimized for specific criteria, like weight reduction or material efficiency.

Additionally, I came across lattice structures, commonly utilized in industries like aerospace to minimize weight or in 3D printing to create infill. Intrigued by the potential, I decided to experiment with designing and printing an object using only lattice structures in Fusion 360  [Software](https://www.ansys.com/applications/topology-optimization). Essentially, I aimed to print just the infill, exploring architectural perspectives where volumetric lattice structures unlock new possibilities in design and aesthetics. 

Moreover, I'm fascinated by ongoing research in [volumetric lattices](https://help.autodesk.com/view/fusion360/ENU/?guid=SLD-CREATE-VOLUMETRIC-LATTICE), which explores new materials, fabrication methods, and applications. This research spans fields from aerospace engineering to biomedical implants, driving innovation across various industries.

While I acknowledge my beginner status in this technology, I'm excited to share my progress from this week.

https://fabacademy.org/2024/labs/barcelona/students/niels-schmidt/3D%20scanning%20and%20printing.html

### Settings
In the following I will focus on FDM printing. When it comes to 3D printing a lot can go wrong. Using benchies or doing the so-called “torture tests” you get information on where to improve your prints. Several parameters are important for the quality of the print and the performance of the 3D printer.

Here are some of the things I learned about settings:

### Strength: 
To set the strength of your print you can use infill and set the thickness of your walls and the top/ bottom. As a standard I use 35% infill density and I use a grid as the infill pattern. I set the wall thickness to about 0.8 mm, and the minimum value is 0.34 mm, with 2 wall thicknesses. For the top/bottom layer, 0.6 mm, and as for the number of layers, I use 5 top and 3 bottom layers.

### Quality:
You can use layer height to adjust print quality. This affects the resolution and the time it takes to print. Higher values give lower resolution and faster printing, and vice versa. 0.2 mm is the value I use as a standard for a nozzle with a diameter of 0.4 mm and a line width of 0.4, because you can use 0.8. [Ru_tutorial_YouTube](https://www.youtube.com/watch?v=EJUU8VPA-cA&ab_channel=voltNik). Here is an example of how it is used:
![Nozzle0.4](/images/week5/nozzle0.8.gif)

### Speed.
I set the speed to 55 mm/s, because this is the best figure indicated in the printer documentation and personally I printed at different rates taking into account different models, where a shaky base is the best option, and where there is good adhesion, you can increase it to 80 mm/s. The printing process has one speed, regardless of whether it is filling or inner/outer wall, because it is necessary to avoid pressure drops during printing (edge curves, overflow, gaps, etc.) and to ensure excellent quality. And the travel speed can be higher (the time the nozzle spends moving between prints). In this example, I set it to 45 mm/sec.

### Temperature:
The temperature of the nozzle affects the melting point of the material. Different materials have different melting points. You can check the setting on the spool. I use 205 degrees for standard PLA. It also depends on the manufacturer, so you need to look at their recommendations, usually these conditions are indicated on their printers and under ideal temperature measurement conditions. There is also the table temperature, which affects the adhesion of the material. I use a 60-degree heating model as a standard, but only on the first layer, then I use 55 degrees during printing.

### Materials:
There are many different types and qualities of filaments, for example: ABS, PLA, Nylon, COPet, PETG, PP,  flexible (TPU/TPE), wood-filled, metal-filled, carbon-filled, PVA (water-soluble), etc.) I use PLA for standard prints and test papers unless I have a specific purpose that requires something special, then I choose something else. Also, I came across an interesting resource and it became useful to me.
- [Materials](https://www.simplify3d.com/resources/materials-guide/)

### Adhesion:
To make sure that print sticks to the bulidplate you can either use brim (a single layer around yor model that should prevent it from warping) or a raft (a thick grid under your model that I would use if the bottom of the model isn’t flat). An advice is not to use any adhesion unless you have to. It can be difficult to remove afterwards.

### Support:
This is about creating structures that support the model and prevents it from collapsing, e.g if it has overhangs and the angle of the overhang is more the 45 degrees (check the torture test). In Cura you can choose between normal (a structure directly below the overhanging parts and straight down) and tree support (a structure like branches that crawl around the model to support it from the buildpalte). The support can be set to be placed everywhere (then the support is printed on the model and has to be removed from the model afterwards which can be tricky and might call for sanding.) or to touch the buildplate.

These are just some of the parameters that can be adjusted and optimized through iterative testing.


## 2. What I was able to achieve
Last week, while making boards on the plotter, I had the idea to improve the vacuum cleaner to better remove chips, because it was not very comfortable due to the large diameter of the pipe. So I came up with a vacuum cleaner nozzle that, with the change of nozzles, is narrowed, at an angle - so you can get into hard-to-reach places. 
This nozzle also fits my vacuum cleaner at home. After all, there is a cone in the middle of the main part.

![3D Project](/videos/week5_project.gif)

![Photo](/images/week5/week5_project.png)

## 3. Problems that arose during the execution of tasks.
### 1) Question for Fusion.
![Question](/images/week5/fusion_prob1.png)

When I built the figure according to a different principle, I had this problem with the thickness of the walls. 
I asked the curator Konstantin for advice, and he told me a lot of interesting things, and it was useful to me. Especially about the construction process, which way to choose, because in my work I compared the two. A much better option, the one that Konstantin pointed out to me, is to build one sketch with all the necessary dimensions (take into account the tolerances) and build the figure by the axis "Solid/Create/Revolve". 

But if you chose this method as I did, then I solved this problem with the help of "Solid/Modify/Shell"

### 2) Repair of 3D Printer.

Because I was having problems with my 3D printer, I decided to repair it.
While calibrating the heating surface, the gap between the nozzle and the table was too large. When I almost achieved a perfect result, one of the 4 corners was maxed out and there was still not enough thread to secure the heating plate and reduce the gap between the nozzle. I decided to turn to my father, as he was an expert and was nearby, so together we chose the best option to solve this problem.
 - Lower the stepper motor with the propeller (as the holes on which it was fixed allowed);
 - Adjust the Z-ends;
 - Review the documentation for this machine ([Read the manual](https://www.manua.ls/anycubic/i3-mega-s/manual?p=16)
 );

We still chose option 3. The documentation said that we needed to adjust the Z-ends.

![Manual Anycubic I3 Mega S](/images/week5/manual.png)


### 3) The setup process leveling:
1. Preheat the table to 70*.
2. Tighten all screws that adjust the clearance between the heating table and the nozzle.

![Manual Anycubic Mega S](/images/week5/bad1.png)

3. Using commands such as Home Z, Move Z and manual mode, move the extruder by hand, visually seeing where the gap is larger and using a screwdriver to tighten/unscrew the end-of-arm Z adjustment screw.
> in the process of calibration, the end stop was replaced with a spare one that came with the kit, because this one behaved badly among others, emitting long-lasting sounds of the triggering signal, but the screw stuck to the point of triggering when it was enough (checked several times, perhaps the adjusting screw was not fully screwed in, but not)

> It is important to configure the two Z-axis endmounts to work together.

![Prossec Calibrate Bed](/videos/calibr2.gif)

4. Leaving a 2-3 mm gap between the nozzle and the heating table, unscrew the clamped screws on the table and calibrate the table with an A4 sheet of paper so that the paper is slightly trapped. In our case, the paper thickness is 0.2 mm.

![Prossec Calibrate Bed](/videos/calibr1.gif)

5. We run the program with the test cube and see the result. In my case, I immediately start printing my parts, checking the first layer to see how it fits and whether the extrusion is sufficient.
