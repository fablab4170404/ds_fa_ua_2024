+++
author = "Dmitri Senchuk"
title = "week 1. Project management"
date = 2024-01-25
description = "Creating, configuring, working on bugs in the Hugo project, Gitlab in creating a website."
tags = [
    "markdown",
    "css",
    "html",
    "themes",
]
categories = [
    "themes",
    "syntax",
]
+++
> At the beginning of my studies at FabAcademy, I was full of enthusiasm and curiosity. The first days were stressful but exciting at the same time. 
  The lectures were full of interesting topics. We learned about various 
technologies, from Git to website development. The presentations by the professors were very impressive, especially those where they shared their experiences and successes.  Together with our fellow students, we felt a strong community spirit at the first lectures.
  Getting to know my team was an interesting challenge. We discussed
ideas, shared experiences, and planned joint steps. I felt that we could achieve great results together.
<!--more-->

## Community of web developers: My experience with Git.

This week I installed the necessary software for the course  such as Git, Go, Node.js, Markdown, Hugo, Microsoft Visual Code and additionally installed Install a C compiler, either GCC. 

  I also created my website with Hugo and hosted it on GitHub Pages. I
added my resume, photo, and contact information to my website + my Fab Academy work. 

  I learned how to use Git  to manage versions of my code, to work more
in the terminal, which I didn't like the idea of at first, but eventually I grew to like it, and how to synchronize it with GitHub. I also learned how to use Markdown to create content for my site and how to use Hugo to generate static HTML files. In achieving the goal, I encountered various difficulties, sometimes obvious, as funny as it sounds, because I was thinking and looking for a completely different reason. 

  There were also problems where I sat for more than one hour to figure
it out, turned to the Git documentation for help, this tutorial, and other members of my team, consulted with Konstantin about the structure of the site, what was possible and what was not. 

## From Git to GitHub Pages: My story.
Documentation: [GitLab](https://docs.gitlab.com/ee/tutorials/make_first_git_commit/), [GitBook](https://git-scm.com/book/en/v2), [Hugo Install](https://gohugo.io/installation/windows/).

Video turorial: [YouTube](https://www.youtube.com/watch?v=Ce5nz5n41z4&ab_channel=GitLab) and [Youtube](https://www.youtube.com/watch?v=8JJ101D3knE&ab_channel=ProgrammingwithMosh)

## **For Windows only!!!**

Step 1: Settings git
I followed the textbook [Git](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week01_principles_practices_project_management/git_simple.html). 

First, download git with:

{{< highlight html >}}
brew install git
{{< /highlight >}}

Set the user configuration for git:

{{< highlight html >}}
$ git config --global user.name "dmitri-senchuk"
$ git config --global user.email "5769650@stud.nau.edu.ua"
{{< /highlight >}}

Check if the SSH key exists:

{{< highlight html >}}
cat ~/.ssh/id_rsa.pub
{{< /highlight >}}

If you do not generate an SSH key:

{{< highlight html >}}
ssh-keygen -t rsa -C "5769650@stud.nau.edu.ua"
{{< /highlight >}}

Check the public key you just created:

{{< highlight html >}}
cat ~/.ssh/id_rsa.pub
{{< /highlight >}}

Step 2. 
- Hugo [install](https://github.com/gohugoio/hugo/releases/tag/v0.122.0)

![Install Hugo](/images/week1/2step2.jpg)

- Via [WinGet](https://apps.microsoft.com/detail/9N0DX20HK701?rtc=1&hl=uk-ua&gl=UA):
 
install hugo on the command line, then write hugo.version on the command line to make sure it is installed and what version it is
The extended version of the hugo code.

- [GO install](https://go.dev/doc/install)

- Install the GCC compiler:
in the Git Bash terminal

{{< highlight html >}}
git clone git://gcc.gnu.org/git/gcc.git SomeLocalDir
{{< /highlight >}}

Update the environment variable as described in the GoPATH documentation
Use the [GCC documentation](https://gcc.gnu.org/git.html) 

![Install GCC](/images/week1/1step2.jpg)

- [Install Hugo for Windows](https://gohugo.io/installation/windows/)

I used this website template:
[Link list gohugo themes](https://themes.gohugo.io/themes/aafu/)


## Problem with the site(

I want to create a new page, but I get the following error:

{{< highlight html >}}
$ hugo new content posts/fabacademy.md 
  Error: failed to load modules: module "aafu" not found in
"C:\\Users\\Dmitri\\fablab\\aafu\\themes\\aafu";
  either add it as a Hugo Module or store it in
"C:\\Users\\Dmitri\\fablab\\aafu\\themes".
{{< /highlight >}}

Now everything is finally perfect)

We generated our logo using Favicon Generator - Text to Favicon - favicon.io, but there were some difficulties when importing it into the project.

The favicon is not working.

Make sure that you have specified the correct type of file with the icon image in the head.html file located in the layouts/partials folder of your Hugo project. You must use the type attribute with the appropriate value, for example, type="image/x-icon" for .ico files, type="image/png" for .png files, type="image/svg+xml" for .svg files, etc. You can find more information about file types here. 

Make sure you are using a compatible browser to view your website. Not all browsers support all image file formats for icons. For example, Internet Explorer doesn't support .png and .svg files for icons, so you must use an .ico file for it. You can check browser compatibility for different image file formats on this page.

It turns out that the necessary data had to be changed in the project folder in a file called config.yaml
The problem of creating a new tab did not yield any progress, so the next day we had a meeting with the team at the Island, FabLab. After talking to one of the team members, Iryna, she helped me change my approach to the problem and look for it elsewhere. 
Having understood the code and logic of the project, we came to the conclusion that in order to create a new page and tab, we need to add it on the front page, here is the path layouts/partials/header.html 
And add a line:

{{< highlight html >}}
<a class="no-underline p-2 rounded hover:bg-gray-200 dark:hover:bg-gray-800" 
href="{{ `fabacademy` | relURL }}">FabAcademy</a>
{{< /highlight >}}

![hugoserver](/images/week1/3step2.jpg)

<link rel="apple-touch-icon" href="3step2.jpg">

Here I will continue to describe the problems I have encountered with my website. Thank you for your attention


## **Why don't photos and videos work?** 
### Windows problems

![Page not found...](/images/week1/problems_content.png)


> If you want the files to be generated by , use the switch. It’s off by default, because rendering in memory is faster.hugo server--renderToDisk.


{{< highlight html >}}
$ hugo server --renderToDisk
{{< /highlight >}}

The thread where this problem and its solution were described [Link](https://discourse.gohugo.io/t/public-folder-and-its-content-not-generated/10535).

## Optimizing Website Images

Since I use the Windows 11 operating system. It has a built-in ability to reduce image size and resolution. I also found an interesting guide for those who use Linux and MacOS.

- [Link FabAcademy](https://fabacademy.org/2018/labs/barcelona/students/krisjanis-rijnieks/assignments/week02/)
- [Stackoverflow](https://stackoverflow.com/questions/14675913/changing-image-size-in-markdown)

### Add Movie in MarkDown
The links that were useful to me were:

- [Stackoverflow](https://stackoverflow.com/questions/46273751/how-can-i-add-a-video-in-markdown)
- 
### For Windows
Click Right Button on the image and select "Resize pictures" and select the size you need. 

![Resize](/images/week1/image_change1.jpg)

Last step

![Resize1](/images/week1/image_change2.jpg)

## Fix the problem with git repository.
How to dealing with this message 'You've added another git repository inside your current repository.
Initially, I already had a repository added, as I built my site through Hugo. So first you need to delete the existing ones and then add your GitLab.
Here's a resource that came in handy for me:
- [Stackoverflow](https://stackoverflow.com/questions/68557504/how-to-dealing-with-this-message-youve-added-another-git-repository-inside-you)
- [Submoluls in a Git repository](https://stackoverflow.com/questions/12641469/list-submodules-in-a-git-repository)

## Hugo migrate to GitLab
- [Nice tutorial](https://fabacademy.org/2018/labs/barcelona/students/krisjanis-rijnieks/assignments/week02/)
- [Detailed Tutorial GitLab](https://docs.gitlab.com/ee/tutorials/hugo/)

I had previously uploaded my site to GitHub via a repository. 

### Add your configuration options
GitLab's documentation describes the next steps:
You specify your configuration options in a special file called . 

To create a new file: 

{{< highlight html >}}
.gitlab-ci.yml
{{< /highlight >}}

1) On the left sidebar, select Code > Repository.
2) Above the file list, select the plus icon ( + ), then select New file from the dropdown list.
3) For the filename, enter . Don’t omit the period at the beginning..gitlab-ci.yml
4) Select the Apply a template dropdown list, then enter “Hugo” in the filter box.
5) Select the result Hugo, and your file is populated with all the code you need to build your Hugo site using CI/CD.

It didn't work for me for several reasons. After all, I use a css preprocessor, the hugo image. I edited the code according to the documentation, but to no avail.

{{< highlight html >}}
default:
  image: "${CI_TEMPLATE_REGISTRY_HOST}/pages/hugo/hugo:0.108.0"
  before_script:
  - apk add --no-cache go curl bash nodejs
  ## Uncomment the following if you use PostCSS. See https://gohugo.io/hugo-pipes/postcss/
  - npm install postcss postcss-cli autoprefixer
{{< /highlight >}}

I asked Irina for help, because she had also encountered this problem.

Here's what you need to do first before the host starts working:
- You need to move all the content from the project folder to the level above where the repository itself and the .gitlab-ci.yml file were located.
- New file/Edit .gitlab-ci.yml and add the following code [documentation](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/): 
- It built the site, created an archive with it ("artifacts" they call it)
- Now you need to go to your io page and check if it has been updated.

{{< highlight html >}}
variables:
  DART_SASS_VERSION: 1.72.0
  HUGO_VERSION: 0.124.0
  NODE_VERSION: 20.x
  GIT_DEPTH: 0
  GIT_STRATEGY: clone
  GIT_SUBMODULE_STRATEGY: recursive
  TZ: America/Los_Angeles

image:
  name: golang:1.22.1-bookworm

pages:
  script:
    # Install brotli
    - apt-get update
    - apt-get install -y brotli
    # Install Dart Sass
    - curl -LJO https://github.com/sass/dart-sass/releases/download/${DART_SASS_VERSION}/dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz
    - tar -xf dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz
    - cp -r dart-sass/ /usr/local/bin
    - rm -rf dart-sass*
    - export PATH=/usr/local/bin/dart-sass:$PATH
    # Install Hugo
    - curl -LJO https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_linux-amd64.deb
    - apt-get install -y ./hugo_extended_${HUGO_VERSION}_linux-amd64.deb
    - rm hugo_extended_${HUGO_VERSION}_linux-amd64.deb
    # Install Node.js
    - curl -fsSL https://deb.nodesource.com/setup_${NODE_VERSION} | bash -
    - apt-get install -y nodejs
    # Install Node.js dependencies
    - "[[ -f package-lock.json || -f npm-shrinkwrap.json ]] && npm ci || true"
    # Build
    - hugo --gc --minify
    # Compress
    - find public -type f -regex '.*\.\(css\|html\|js\|txt\|xml\)$' -exec gzip -f -k {} \;
    - find public -type f -regex '.*\.\(css\|html\|js\|txt\|xml\)$' -exec brotli -f -k {} \;
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
{{< /highlight >}}

Job result:

{{< highlight html >}}
$ hugo --gc --minify
Start building sites … 
hugo v0.124.0-629f84e8edfb0b1b743c3942cd039da1d99812b0+extended linux/amd64 BuildDate=2024-03-16T15:44:32Z VendorInfo=gohugoio
                   | EN  
-------------------+-----
  Pages            | 95  
  Paginator pages  |  3  
  Non-page files   |  1  
  Static files     | 73  
  Processed images |  0  
  Aliases          | 41  
  Cleaned          |  0  
Total in 1759 ms
$ find public -type f -regex '.*\.\(css\|html\|js\|txt\|xml\)$' -exec gzip -f -k {} \;
$ find public -type f -regex '.*\.\(css\|html\|js\|txt\|xml\)$' -exec brotli -f -k {} \;
Uploading artifacts for successful job
00:08
Uploading artifacts...
public: found 676 matching artifact files and directories 
WARNING: Upload request redirected                  location=https://gitlab.com/api/v4/jobs/6411581068/artifacts?artifact_format=zip&artifact_type=archive new-url=https://gitlab.com
WARNING: Retrying...                                context=artifacts-uploader error=request redirected
Uploading artifacts as "archive" to coordinator... 201 Created  id=6411581068 responseStatus=201 Created token=glcbt-65
Cleaning up project directory and file based variables
00:01
Job succeeded
{{< /highlight >}}

### Error ! Remove Rejected main - main
When pushing my project to the site, I encountered this problem, and I found the solution to it by following this path and adding rights GitLab:
Repository/Protected branches/Add protected branches

![Brunches](/images/week1/brunches.png)

![Rules](/images/week1/rules.png)

This problem is also described on the forum: 
- [Link](https://stackoverflow.com/questions/21407962/error-on-git-push-remote-rejected-master-master-pre-receive-hook-decli)

### When using a baseURL with path, links to static resources don't include path (but CSS, JS and page links do)
- [GitHub](https://github.com/gohugoio/hugo/issues/8078)

### How to embed a video in Markdown?

- [Intresting blog.markdown](https://blog.markdowntools.com/posts/how-to-embed-a-video-in-markdown)

MarkDown does not support videos, only .gif. Therefore, you can add a link to a video on YouTube or Vimeo as an option. There is a great site that generates ready-made code from your link to the video. Just enter your link and a caption there. This is what the generated code looks like:

- [MarkDown Videos](https://markdown-videos.jorgenkh.no/)

{{< highlight html >}}
![[![Machine](https://markdown-videos-api.jorgenkh.no/url?url=https%3A%2F%2Fvimeo.com%2F945892477%3Fshare%3Dcopy)](https://vimeo.com/945892477?share=copy)]
{{< /highlight >}}

### Git Push

{{< highlight html >}}
$ cd "project"
$ git status
$ git add .
$ git commit -m "message"

If you update something that was local:
$ git push -u origin main

And if you update something by hand right on the gitlab, it's afterwards:
$ git pull origin main

Completely overwrite everything with your local everything:
$ git push -uf origin

"f" is for "force", which means that it overwrote your remoter repository with your local everything by force.
If you didn't have those last changes in gitlab in the local version, they will disappear.
{{< /highlight >}}

Good Luck!