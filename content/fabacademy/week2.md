---
title: "week 2. Computer Aided design"
tags: ["Fusion360", "3D", "2D", "drawing"]
categories: ["CAD", "CAM"]
description: "Creation of drawings and 3D models in the CAD system"
date: 2024-01-31
draft: false
---
# THE ASSIGNMENT:
# 1. EVALUATE AND SELECT 2D AND 3D SOFTWARE & DEMONSTRATE AND DESCRIBE PROCESSES USED IN MODELLING WITH 2D AND 3D SOFTWARE.

# 2. MODEL EXPERIMENTAL OBJECTS/PART OF A POSSIBLE PROJECT IN 2D AND 3D SOFTWARE

My main position is that there are advantages to choosing open source software: it is free, has an active community for development and support, and the source code is open for customization. This reflects a democratic mindset. Proprietary software also has its advantages, especially in terms of available features, professional development, and ease of use.

My choice is also driven by the possibility of effectively integrating the software into our work in makerspaces at Ostriv. I will consider the availability of the software at my university and its usability for students and teachers.

# MY CHOICE OF 2D AND 3D SOFTWARE:

1. Fusion 360

2. Inkscape

3. Paint

4. Tinkercad

## ON 3D MODELLING
- NURBS (nonlinear rational basis splines) are used to create curves and surfaces. They use control points to adjust the surface and ensure smoothness. It is an accurate method that takes up less space and is easily transferred between programs.

- Mesh - uses control points on the X, Y, Z axis. The use of polygons makes it easy to manipulate the model. Mesh programs are an excellent starting point for learning the basics of 3D modeling. They can be directly converted to STL format, which is a benefit when you are working with 3D printing.

## ON 2D MODELLING
When creating 2D models, either vectors (lines) or pixels (points/squares) are used. Vectors can be scaled without restriction while maintaining quality, provide smaller file sizes, and can be processed on less powerful computers. Primitive geometric shapes can be combined to form more complex shapes. Vectors are created with a vector graphics program and are useful for working with a vinyl cutter and laser cutter.

Pixels are also known as raster images and are the smallest unit of a raster image. Each pixel is made up of RGB subpixels. As the image size increases, pixelation (loss of quality) occurs, which leads to an increase in file size and may require compression, especially for use on the web. Raster images are created with devices such as cameras and scanners or with raster graphics programs.

## MY EXPERIMENTAL OBJECT
I kept thinking about how to create a safety device to start a small toy car from a mockingbird. So I decided to print the model on a 3D printer.

- Sketch
![Sketch](/images/week2/sketch1.png)
![Sketch](/images/week2/sketch2.png)
![Sketch](/images/week2/sketch3.png)

My experiment is to model a fitting using Fusion 360.

# These are the steps with the dimensions:
### 1 Part
Let's start building models with the body 
- Sketch 
![part1](/images/week2/part1_1.png)

- Extrude object
![part1_2](/images/week2/part1_2.png)

- Create a drawing on the constructed object
![part1_3](/images/week2/part1_3.png)

- We perform the same extrusion operation
![part1_4](/images/week2/part1_4.png)

- Done!
![part1](/images/week2/part1)

The next part of the project
### 2 Part
- After making the drawing, we extrude and make a groove for the ring.
![part2](/images/week2/part2.png)

### 3 Part
- We build the ring according to the drawing and extrude it.
![part3](/images/week2/part3.png)

### Assembly
- Assembly parts
![assembly_parts](/images/week2/assembly_parts.png)

- Render
![render](/images/week2/render.png)




