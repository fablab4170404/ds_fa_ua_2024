---
title: "week 4. Electronic Production"
tags: ["CAD", "CAM", "fiberlaser", "milling"]
categories: ["milling", "electronic-production", "fiberlaser"]
description: "PCB production process"
date: 2024-02-14
draft: false
---

# Production PCB

THE ASSIGNMENT:
1. [GROUP ASSIGNMENT](https://gitlab.com/ssavito1122/ds_fa_ua_2024)

![Groupwork](/images/week4/gr_work.png)

2. INDIVIDUAL ASSIGNEMENT:

> - MAKE AND TEST THE DEVELOPMENT BOARD THAT YOU DESIGNED TO INTERACT AND COMMUNICATE WITH AN EMBEDDED MICROCONTROLLER
> - REFLECT ON PCB PRODUCTION PROCESS

## Milling PCB
I have used the ready-made files for swd+uart adapter+hello board - xiao rp2040 by this student - Adrián Torres:
- [Link fabcloud](https://gitlab.fabcloud.org/pub/programmers/quentorres/-/tree/main?ref_type=heads)

![Png file](/images/week4/quentorres_traces.png)

![Відео процесу виробництва](/videos/week4_1.gif)


1. [Mods for PCB's tutotial](https://pub.fabcloud.io/tutorials/week04_electronic_production/mods.html)

![CAM](/images/week4/mods1.png)

![Parametrs in mods](/images/week4/mods_param.png)


![Відео процесу виробництва](/videos/week4_2.gif)

Надломився кінчик фрези, перевірили наскільки він вплине на якість обробки.

![Перша спроба](/images/week4/pcb_crash.png)

![Друга спроба](/images/week4/pcb_mill.png)


## FiberLaser PCB

![Grav PCB](/images/week4/i_week4.jpg)

I checked the quality through a microscope, there were chips in the grooves
![Microscope](/images/week4/microscope1.png)
![Microscope](/images/week4/microscope2.png)

## Repair Machine 

![Repair Machine](/images/week4/repair_machine.png)'

![Repair Machine](/videos/repair_machine.gif)


