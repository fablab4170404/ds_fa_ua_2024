---
title: "Final Project"
tags: ["navigation", "notes", "device", "safety"]
categories: ["navigation", "myproject"]
description: "This describes my final project at FabAcademy"
date: 2024-01-24
draft: false
---

# ShpakUA
## History The name comes from:

### Common starling (Sturnus vulgaris): 
This bird belongs to the starling family and lives in a variety of environments, from rural to urban. 
It is known for its incredible singing skills, which it uses to attract mates, 
scaring off rivals, or communicating with other birds. The starling can also imitate the voices of other birds,
animals, or even human sounds such as bells, sirens, or music. 
The starling does not alert others to danger, but it can accompany them to safety,
when it flies in large flocks called murmurations. 
These flocks consist of thousands of birds that perform complex manoeuvres in the air, 
to avoid predators or find a favourable place to spend the night.

## What is the goal of the project?
To enable Ukrainians to move to a safe place anywhere in the country during the Russian threat in the sky, as we know in the house
It is not always safe in the house where you live, because the enemy knows no mercy and demonstrates it every day by hitting residential buildings with all types of weapons.
These shelters will be able to provide assistance, as well as in the invincibility centres where people can receive medical and psychological assistance at any time, 
warm tea and food, and charge gadgets, as these centres are equipped with generators.

![device](/images/finalproject/device.png)

It is also necessary to help foreigners who are still poorly orientated to ensure their safety. Therefore, a pocket device will be
to see which shelter is near you, where you can get help and how to get there. 

![Side view](/images/finalproject/solider_and_device.png)

The device will already have up-to-date maps and directions downloaded, and an additional feature is GPS, which, if enabled
will search for places near you.
Maps and addresses are updated via Wi-Fi.

## What it consists of?

The screen is perfect for my device:
- [Link SeedStudio](https://www.seeedstudio.com/Seeed-Studio-Round-Display-for-XIAO-p-5638.html)

And this is if someone needs to accurately determine the presence of a person nearby. 
It can also be useful for others who have ideas for development:
- [Link SeedStudio](https://www.seeedstudio.com/Seeed-Studio-24GHz-mmWave-for-XIAO-p-5830.html)

While searching, I found a ready-made API for the project developed by the Ukrainian team:
- [Link API](https://misto.lun.ua/shelters?ls=shelters-private%2Cshelters-municipal%2Cshelters-other#7.59/50.42/30.285)


гарне джерело якісного україномовного матеріалу по багатьом темам ФабАкадеміі - це Толокарова МейкерАкадемія 
- https://tolocar.org/ua/academy/
раджу ознайомитись - обʼємна репрезентація моделей - це математична база для еффективного користування можливостями аддитивного виробництва
- https://researchoutreach.org/articles/volumetric-representations-revolutionise-3d-printing/





