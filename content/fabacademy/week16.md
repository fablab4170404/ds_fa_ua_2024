---
title: "week 16. Wildcard week"
tags: ["machinebuild", "coding", "communication"]
categories: ["coding", "board programming", "machinebuild"]
description: "We were engaged in creating a machine for string cutting for polyfoam"
date: 2024-05-13
draft: false
---

# THE ASSIGNMENT:
1. GROUP ASSIGNMENT:
- [Work on the assembly and development of the project](https://fabacademy.org/2024/labs/ostriv/)

[![week16](https://markdown-videos-api.jorgenkh.no/url?url=https%3A%2F%2Fvimeo.com%2F945957698%3Fshare%3Dcopy)](https://vimeo.com/945957698?share=copy)

![week16_1](/images/week16/general.jpeg)

This is a general view of our working string cutter.

![week16_2](/images/week16/group1.jpeg)
It is switched on with a conventional AC/DC switch, which controls the degree of glow of the string.

![week16_3](/images/week16/group2.jpeg)

Having a ready-made pen plotter and a string cutter, we combined 
them and made a bracket with a claw that holds the foam, and we did 
it all in a short time, without having to think about and model complex
mechanisms for a long time. 

First sketch:
![week16_4](/images/week16/add1.png)

The second:
![week16_5](/images/week16/add2.png)

Two additional sketches were developed in Fusion360, the first was to connect two machines where they fit together perfectly and are easy to disassemble if they need to be transported. And the other sketch is the one that we will cut on our string cutter.

Connecting two machines to each other:

![week16_6](/images/week16/add.png)


Next, we will use Inkscape software to get g-code through a plugin that we will install:

- [GitHub](https://github.com/costycnc/costycnc-inkscape-1.0)

No problem to install it on Mac, Linux and Windows

This tutorial was helpful in installing this plugin:

[![week16_tutorial](https://markdown-videos-api.jorgenkh.no/url?url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D51S8nDTFHPA%26ab_channel%3DBoboacaCostel)](https://www.youtube.com/watch?v=51S8nDTFHPA&ab_channel=BoboacaCostel)

### How to use the plugin:

![week16_7](/images/week16/panel1.png)

The size of your sketch can be changed with the help of dpi

![week16_8](/images/week16/panel2.png)

Copy only g-code.

![week16_9](/images/week16/panel3.png)

Next, we use this site to generate our sketch with inserts.

- [Intresting guide](https://www.instructables.com/Create-a-File-for-Foam-Cutter-Machine/)

Now your gcode is in clipboard and ready to paste in a text editor like notepad or can go to paste in gcode box

I prefer to create a box with gcode for copy to clipboard instead save gcode file because inkscape when want to save file in your computer and not have privilege the file cant be saved and you need to set as administrator access to inkscape. Is a procedure little complicated and my solution with copy and paste is easy and no create problems

- [costycnc](www.costycnc.it/cm8)

Alternatively, you can not send our copied g-code to this site, but immediately open our Universal G-code Sender and paste this code in the editor, but our string cut is arranged in such a way that you have to change the "y" position to negative. We'll rework the firmware later so that you don't have to do this every time.

We monitor the orientation in which direction X the process will take place, zeroing before the workpiece. 
Feed rate = 200-400.

![week16_10](/images/week16/sender.png)

In the photo, our insert is highlighted in red. It is placed according to the position where our sketch was in Inkscape. In my case, it was x=5;y=5, so the corresponding short insertion is visible in the G-code.

Connecting to our machine, you first need to press the Home button or go to machine 0. After that, you can drive up to our string. Be careful, if you disconnect from the machine and reconnect to the machine and your workpiece is near the string, the machine will not move unless you go to Home, and if you do, you will hit the string and it will break.

Code example:

{{< highlight html >}}
G21 ; millimeters
G90 ; absolute coordinate
G17 ; XY plane
G94 ; units per minute feed rate mode

; Go to zero location
G0 X0 Y0

; Create U text
G21 F400 G90
G92 X0 Y0
G01 X0.00 Y0.00
G01 X1.16 Y-1.16
G01 X1.16 Y-32.06
G01 X1.16 Y-32.06
G01 X17.16 Y-41.30
G01 X33.16 Y-32.06
G01 X33.16 Y-1.16
G01 X25.16 Y-1.16
G01 X25.16 Y-28.68
G01 X17.16 Y-33.30
G01 X9.16 Y-28.68
G01 X9.16 Y-1.16
G01 X1.16 Y-1.16
G01 X0.00 Y0.00
G01 X0 Y0
{{< /highlight >}}

## Firmware

- [GitHub](https://github.com/ikae/MI-GRBL-Z-AXIS-Servo-Controller)

## Links to machines

1) The GitLab link provided directs to a repository named “Pen Plotter” on 
GitLab. This repository contains code, designs, or documentation 
related to building or using a pen plotter.
If you’re interested in creating your own pen plotter or learning more 
about this type of machine, you can explore the GitLab repository for detailed information.

- [GitLab | Pen Plotter](https://gitlab.com/tolocar/pen-plotter)

![week16_11](/images/week16/pen_plotter.png)

2) The second image depicts a hot wire cutter, a tool commonly used in crafting, model making, 
and other creative projects.
The GitLab link points to a repository named “Hot Wire Cutter” on GitLab. Inside this repository, 
you may find resources related to building, using, or improving a hot wire cutter.
Hot wire cutters are often used to precisely cut foam, polystyrene, or other materials by heating 
a wire to a high temperature and then slicing through the material.
If you’re interested in DIY projects or want to explore how hot wire cutters work, the GitLab 
repository might provide valuable insights.
Feel free to explore the GitLab links for more detailed information about these machines! 🛠️🔗

- [GitLab | Hot Wire Cutter](https://gitlab.com/tolocar/hot-wire-cutter)

![week16_12](/images/week16/wire_cutter.png)