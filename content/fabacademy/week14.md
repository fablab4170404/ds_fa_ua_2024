---
title: "week 14. Networking and communications"
tags: ["PCB", "coding", "ESP32", "communication"]
categories: ["coding", "board programming", "PCB"]
description: "Setting up communication between two ESP32C3 devices"
date: 2024-04-27
draft: false
---

# THE ASSIGNMENT:
1. GROUP ASSIGNMENT:
- [Compare as many tool options as possible](https://gitlab.com/ssavito1122/ds_fa_ua_2024)

2. INDIVIDUAL ASSIGNEMENT: 
- Write an application that interfaces a user with an input &/or output device that you made.
      
## How connecting board. 
I used intresting tutorial:
 - [Esp32 Arduino IDE](https://randomnerdtutorials.com/esp-now-esp32-arduino-ide/)
 - [Download Arduino IDE](https://www.arduino.cc/en/software)
 - [Tutorial Installing CP210x USB to UART Bridge VCP Drivers On Windows](https://thenovicesensor.com/installing-cp210x-usb-to-uart-bridge-vcp-drivers-on-windows/#:~:text=Go%20to%20https%3A%2F%2Fwww.silabs.com%2Fdevelopers%2Fusb-to-uart-bridge-vcp-drivers.%20Navigate%20to%20the%20downloads%20section,drivers.%20Save%20the%20driver%20in%20a%20convenient%20location.)

To establish communication between the two devices, you first need to select the software environment,
in our case it is Arduino IDE, and install the necessary drivers.

To do this, I will use two [Seeed Studio XIOA ESP32C3](https://www.seeedstudio.com/Seeed-XIAO-ESP32C3-p-5431.html) controllers.

1.1. In your Arduino IDE, go to File> Preferences

![week14_1](/images/week14/week14_1.png)

1.2. Enter the following into the “Additional Board Manager URLs” field:

{{< highlight html >}}
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
{{< /highlight >}}

1.3. Then, click the “OK” button:

1.4. Open the Boards Manager. Go to Tools > Board > Boards Manager

![week14_2](/images/week14/week14_2.png)

1.5. Search for ESP32 and press install button for the “ESP32“ by Espressif Systems:
- [Documentation GitHub](https://github.com/espressif/arduino-esp32)

That's it. It should be installed in a few seconds depending on your computer.

2. Testing the Installation

2.1. Plug the ESP32 board to your computer. With your Arduino IDE open, follow these

2.2. Select your Board in Tools > Board menu (in my case it’s the XIAO-ESP32C3)

![week14_3](/images/week14/week14_3.png)

2.3. Select the Port (if you don’t see the COM Port in your Arduino IDE, you need to install the CP210x USB to UART Bridge VCP Drivers):
!!Attention!! It's a common mistake when a TYPE-C cable may not be suitable for charging your phone.

- [Installing CP210x USB to UART Bridge VCP Drivers On Windows](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers?tab=downloads)

I installed this driver, because there is an .exe file "CP210x Windows Drivers" 
and "CP210x Windows Drivers with Serial Enumerator"

## The process of communication

1. First, you need to find out our MAC address of the board.
Connect the Type-C to the board, hold down the "B" button and do not release it, 
insert the USB into the laptop, and release "Boot".

Sometimes you can't connect correctly, so see the Errors section for how to fix it.

2. We worked together with Iryna in a team. The code to be filled in by the recipient, me:

{{< highlight html >}}
#include <esp_now.h>
#include <WiFi.h>

// Structure example to receive data
// Must match the sender structure
typedef struct struct_message {
    int b;
} struct_message;

// Create a struct_message called myData
struct_message myData;
int ledPin = D10; // пін для ЛЕД

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("Bytes received: ");
  Serial.println(len);

  Serial.print("Led state value: ");
  Serial.println(myData.b);
  Serial.println();

  digitalWrite(ledPin, myData.b); // передається лед на цей сигнал дата Б
}

void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);
  
  pinMode(ledPin, OUTPUT);
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);
}
 
void loop() {

}
{{< /highlight >}}

3. The code to be filled in by the sender, Iryna:

{{< highlight html >}}

#include <esp_now.h>
#include <WiFi.h>

// REPLACE WITH YOUR RECEIVER MAC Address
//d4:f9:8d:04:11:e4 Tanya
//D4:F9:8D:04:2A:20 Dima LED
uint8_t broadcastAddress[] = {0xD4, 0xF9, 0x8D, 0x04, 0x2A, 0x20};

// Structure example to send data
// Must match the receiver structure
typedef struct struct_message {
  int b; // 1 or 0 for LED value
} struct_message;

// Create a struct_message called myData
struct_message myData;

esp_now_peer_info_t peerInfo;

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}
 
void setup() {
  // Init Serial Monitor
  Serial.begin(115200);
 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
}
 
void loop() {
  // Set values to send
  int ledState = 0;

  // if we read anything from serial monitor
  if(Serial.available() > 0) {
    //reading value (we should use only 0 or 1)
    ledState = Serial.parseInt();
    Serial.println(ledState); //checking what was read

    myData.b = ledState; // put led state value into our data structure
    
    // Send message via ESP-NOW
    esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &myData, sizeof(myData));
      
    if (result == ESP_OK) {
      Serial.println("Sent with success");
    }
    else {
      Serial.println("Error sending the data");
    }
  }
}
{{< /highlight >}}

4. A message is sent from the first device with "1" for switching on the diode, and "0" for switching off

### Sender:

![week14_6](/images/week14/week14_6.png)

### Recipient: 

![week14_5](/images/week14/week14_5.png)

### Movie:
![week14_7](/videos/week14_7.gif)

![week14_8](/videos/week14_8.gif)

## Errors
A problem occurred while compiling the firmware. 
In this case, you need to first hold down the "B" Boot button on the board, 
then "R" (Reset) for 1-3 seconds, hold and release it, and then release "B".

![week14_er1](/images/week14/week14_er1.png)
