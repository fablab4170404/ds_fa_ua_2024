---
title: "week 6. Embedded programming"
tags: ["PCB", "coding", "circuit_python", "RasberryPI"]
categories: ["coding", "board programming", "PCB"]
description: "Programming the RasberryPI board"
date: 2024-02-28
draft: false
---

# THE ASSIGNMENT:
1. GROUP ASSIGNMENT:[BROWSE THROUGH THE DATA SHEET FOR YOUR MICROCONTROLLER AND COMPARE THE PERFORMANCE AND DEVELOPMENT WORKFLOWS FOR OTHER ARCHITECTURES](https://gitlab.com/ssavito1122/ds_fa_ua_2024)

2. INDIVIDUAL ASSIGNEMENT: 
- WRITE A PROGRAM FOR A MICROCONTROLLER DEVELOPMENT BOARD THAT YOU MADE, TO INTERACTE AND COMMUNICATE

## How I programmed and customized the board. 
First, we install the necessary software and prepare our tools for this.

I also found it useful to know how to install it correctly, so I'm sharing the documentation with you.
- How to Install [Curcit Python Tutorial](https://learn.adafruit.com/welcome-to-circuitpython/installing-circuitpython) for RP2040-Zero boards. When I did my homework, the current version was v8.2.10. This is the [latest stable](https://circuitpython.org/board/waveshare_rp2040_zero/) release of CircuitPython that will work with the RP2040-Zero. 
- How to Install [Mu Editior](https://codewith.mu/en/howto/1.2/install_windows)
Mu is a simple code editor that works with the Adafruit CircuitPython boards. It's written in Python and works on Windows, MacOS, Linux and Raspberry Pi. The serial console is built right in so you get immediate feedback from your board's serial output!
- How to use the online editor code [Code Curcit Python](https://code.circuitpython.org/)
You can use the online Code Curcit Python https://code.circuitpython.org/ to simplify your work and choose how to connect to the board, Wi-Fi, Bluetooth, USB port.

![Photo](/images/.png)

The setup: I connected the Seed to the PC (press and hold the Boot button) and made sure the "RPI-RP2" disk was shown on the PC. Now the hardware was connected. Then I added Seeed Studio XIAO RP2040 board to my CircuitPython.

Then I installed it from the Boards manager and selected the board and the port.