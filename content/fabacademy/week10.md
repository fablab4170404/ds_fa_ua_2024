---
title: "week 10. Machine design"
tags: ["project", "3D_printing", "cutting", "modeling", "Fusion360"]
categories: ["groupproject", "3D_printing"]
description: "A group project to recycle plastic into filament for a 3D printer."
date: 2024-03-30
draft: false
---

# THE ASSIGNMENT:
1. GROUP ASSIGNMENT:[BROWSE THROUGH THE DATA SHEET FOR YOUR MICROCONTROLLER AND COMPARE THE PERFORMANCE AND DEVELOPMENT WORKFLOWS FOR OTHER ARCHITECTURES](https://fabacademy.org/2024/labs/ostriv/)

[![Machine](https://markdown-videos-api.jorgenkh.no/url?url=https%3A%2F%2Fvimeo.com%2F945892477%3Fshare%3Dcopy)](https://vimeo.com/945892477?share=copy)

During this week, we worked on the PET Plastic Recycler project and constantly refined it.
## Problems that have arisen:
1. Machine parameters
Solution:
- First, we made a comb (without applying tolerances in the RDworks Offset CAM system...)
- We chose the option that would satisfy us (7.9) for a thickness of 8 mm plywood
- Then we applied the tolerances in the CAM system to our models.

![Photo](/images/week10/rdworks.png)
![Photo](/images/week10/group_laser.png)

## The assembly process:

![Photo](/images/week10/assembly.png)
![Movie](/videos/assembly1.gif)

2. Problem with the assembly.
1) The grooves do not go down (the front part is longer to make grooves, as on the back of the part)

![Photo](/images/week10/connector1.png)

2) The holes for the ON/OFF button do not match, a 20 mm hole was drilled.

![Photo](/images/week10/buttom.png)

3) It turned out that there were missing details.

![Photo](/images/week10/miss1.png)
![Photo](/images/week10/miss2.png)

4) There are not enough holes on the table to mount the bed.

![Photo](/images/week10/table.png)

Solution:
We came to a common conclusion that we need to first assemble one wall by attaching them to it,
and then in the upper left corner with a slight indentation from the edge they can be attached.
After watching the video of the complete assembly of the project again, you can also focus on
the square hole on the table that is intended for the wires of the stepper motor.

5) The wiring diagram for the electrical part is missing.

![Photo](/images/week10/electr.png)

6) All nuts and bolts are missing.

7) The screws for the motor mounting are too long for the screwdriver

![Photo](/images/week10/screw1.png)

and 

![Photo](/images/week10/screw5.png)

8) Screws for mounting the motor on M3 16 mm are required.

![Photo](/images/week10/screw2.png)

9) Larger holes are needed here to make it easier to twist.

![Photo](/images/week10/screw3.png)
![Photo](/images/week10/screw4.png)

10) Determine the orientation of the print, because there is a problem with assembly.

![Photo](/images/week10/orient.png)

11) This assembly needs attention - there is no place for the frame mount bolt and the lever needs an additional spacer from the frame.

![Photo](/images/week10/frame.png)
![Photo](/images/week10/frame1.png)

12) The recessed hole must be on the bar that is screwed to the lever, not on the lever itself. Other parts also have additional spaces for the nut.

![Photo](/images/week10/lever.png)

13) Perfect holes for bearings.

![Photo](/images/week10/bearing.png)

14) The mechanism works.

![Movie](/videos/lever1.gif)

15) Important. Details for cutting plastic. It should be noted that you must first tighten 
the nuts and bolts, then press the bearings (pay attention to the chamfer and cutting edge as in the photo)
If you press the bearing on the wrong side, you will not be able to disassemble it if 
you try to disconnect it - the plastic will not withstand such loads.

![bearing1](/images/week10/bearing1.png)

![bearing2](/images/week10/bearing2.png)

16) Bracket for the heating head with a nozzle - the hole was drilled for 6 mm, but it was 5.6 mm

## Connect Board
![Board](/images/week10/board.png)

3. Additional links to files:
- [Google Drive](https://drive.google.com/drive/folders/1qZetjbWUJT2vL3A31-xEZem82swEiiDW?usp=share_link)
- [Original](https://drive.google.com/drive/folders/1OutyvpTKozzKmUf_CGHycf-H5AB3hxcR)

# Windows/Firmware
1. After downloading the necessary files and the program for flashing this device, 
pay attention to the path where they are located so that the path does not contain Cyrillic characters.
2. Check port board in Device Manager. In my case, it's COM4
The board port was not displayed because I had a mouse connected.
3. Software for programming 
### Arduino IDE 2.3.2
Select Board --> Mega or Mega 2560
 
{{< highlight html >}}
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(1000);                      // wait for a second
  digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
  delay(1000);                      // wait for a second
}
{{< /highlight >}}

or

{{< highlight html >}}
#include <EEPROM.h>
void setup() { for (int i = 0 ; i < 512 ; i++) {EEPROM.write(i, 255);}}
void loop() {}c:\Users\dmitr.DMITRI-PC\test\2021.11.15_StanokDlyaPET_Butilok.ino.eightanaloginputsV3.34.hex
{{< /highlight >}}

### gcUploader 
- [Download](https://github.com/KyrychenkoYevhen/avr_soft_tools/tree/master/gcUploader)
- Open file "gcUploader.ini"
But specify your path to the firmware file.
Enter the following lines and save the file.

{{< highlight html >}}
[Mega 2560]
avrdude_param=-F -v -p m2560 -c wiring -P %1 -b 115200 -D -U flash:w:"%2":i
port=COM4
hex_file_path=C:\Users\dmitr.DMITRI-PC\test\2021.11.15_StanokDlyaPET_Butilok.ino.eightanaloginputsV3.34.hex
{{< /highlight >}}

or 

![board](/images/week10/board.png)

Intresting info [this website](http://www.getchip.net/posts/148-avtomaticheskijj-zagruzchik-hex-fajjlov-dlya-avr-mikrokontrollerov-obnovlenie-gcuploader/)
- Open file "gcUploader.exe"
Type: Mega 2560
Hex: 2021.11.15_StanokDlyaPET_Butilok.ino.eightanaloginputsV3.34.hex
- Сopy file path -  Ctrl+V dialog window
- Click "art Chip" for Start 
If the attempt is unsuccessful, it may also help to reboot the board via the button on it.

![prog](/images/week10/board_prog.png)