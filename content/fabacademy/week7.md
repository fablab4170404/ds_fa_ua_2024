---
title: "week 7. Computer Controlled Machine"
tags: ["milling", "CNC", "Fusion360"]
categories: ["furniture manufacture", "CAD", "CAM", "CNC"]
description: "The process of creating furniture with parts interconnected without glue and other additional connections"
date: 2024-03-06
draft: false
---

# THE ASSIGNMENT:
1. GROUP ASSIGNMENT:[BROWSE THROUGH THE DATA SHEET FOR YOUR MICROCONTROLLER AND COMPARE THE PERFORMANCE AND DEVELOPMENT WORKFLOWS FOR OTHER ARCHITECTURES](https://gitlab.com/ssavito1122/ds_fa_ua_2024)

2. INDIVIDUAL ASSIGNEMENT: 
- WRITE A PROGRAM FOR A MICROCONTROLLER DEVELOPMENT BOARD THAT YOU MADE, TO INTERACTE AND COMMUNICATE

https://www.youtube.com/watch?v=JCXGjD0Rpg8&list=PLjF7R1fz_OOXWHQhEVEI5Jqf18TQRr5Hu&ab_channel=AdafruitIndustries

https://www.kicad.org/

https://www.tinkercad.com/

https://www.digikey.com/en/resources/conversion-calculators/conversion-calculator-smd-resistor-code

https://everycircuit.com/

https://blog.adafruit.com/2024/01/06/revolutionize-your-esp32-projects-with-live-gpio-pin-monitoring/

https://fabacademy.org/2024/labs/bangor/assignments/week05/

https://github.com/easyw/kicadStepUpMod/

https://sschueller.github.io/posts/ci-cd-with-kicad-and-gitlab/

What do you think about AI to do PCB's
Do you recommend? I find https://www.flux.ai/p/

Yeah, you can export Gerber and to Mods now

You can export to KiCad and take it from there

you can also sell your electronics on https://www.tindie.com/

Logic Analyzer documentation https://fabacademy.org/2020/labs/leon/students/adrian-torres/week06.html#logic

If anyone wants to see what weird things look like in a logic analyzer I had to use one quite a bit to debug my i2c connection https://fabacademy.org/2022/labs/fct/students/ricardo-marques/assignments/week11-2/

We created another variation of the QuenTorino board for our fabrication process. Introducing Tarantino: https://gitlab.fabcloud.org/pub/programmers/tarantino