---
title: "week 15. Interface and application programming"
tags: ["PCB", "coding", "ESP32", "communication"]
categories: ["coding", "board programming", "PCB"]
description: "Setting up communication between two ESP32C3 devices"
date: 2024-04-27
draft: false
---

# THE ASSIGNMENT:
1. GROUP ASSIGNMENT:
- [Compare as many tool options as possible](https://gitlab.com/ssavito1122/ds_fa_ua_2024)

2. INDIVIDUAL ASSIGNEMENT: 
- Write an application that interfaces a user with an input &/or output device that you made.
      
## How connecting board. 
I used intresting tutorial:
 - [Esp32 Arduino IDE](https://randomnerdtutorials.com/esp-now-esp32-arduino-ide/)
 - [Download Arduino IDE](https://www.arduino.cc/en/software)
 - [Tutorial Installing CP210x USB to UART Bridge VCP Drivers On Windows](https://thenovicesensor.com/installing-cp210x-usb-to-uart-bridge-vcp-drivers-on-windows/#:~:text=Go%20to%20https%3A%2F%2Fwww.silabs.com%2Fdevelopers%2Fusb-to-uart-bridge-vcp-drivers.%20Navigate%20to%20the%20downloads%20section,drivers.%20Save%20the%20driver%20in%20a%20convenient%20location.)

To establish communication between the two devices, you first need to select the software environment,
in our case it is Arduino IDE, and install the necessary drivers.

To do this, I will use two [Seeed Studio XIOA ESP32C3](https://www.seeedstudio.com/Seeed-XIAO-ESP32C3-p-5431.html) controllers.

1.1. In your Arduino IDE, go to File> Preferences

![week14_1](/images/week14/week14_1.png)

1.2. Enter the following into the “Additional Board Manager URLs” field:

{{< highlight html >}}
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
{{< /highlight >}}