# fabblog Theme

The fabblog Theme is a responsive portfolio with blog.

## Installation

```shell
git clone https://github.com/s8/FabAcademyUA_2024_Ostriv/tree/main/Dmitri_Senchuk/fabblog.git
cd fabblog
npm install
hugo server
```

## Getting started

After cloning the fabblog repo, modify the `config.yaml` as you wish.

### The config file

You'll find a file called [`config.yaml`](//FabAcademyUA_2024_Ostriv/tree/main/Dmitri_Senchuk/fabblog/blob/master/config.yaml). Customize it per your need.

Note that the sections to be displayed in the accordion, the order of the sections, and the section that should be expanded at the beginning can be specifed in the `config.yaml`.

### Add your photo

Go to `static/images` and replace the `profile.jpg` with your own file.

### Theme Colors

The `fabblog` theme provides `light` and `dark` theme.